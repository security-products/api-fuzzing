## API security

This project contains released images for a GitLab API Security analyzer and was configured as per https://gitlab.com/gitlab-org/gitlab/-/issues/330671 and https://gitlab.com/gitlab-org/gitlab/-/issues/297525#group-and-project-settings.

### Relevant links

- Source code: https://gitlab.com/gitlab-org/security-products/analyzers/api-fuzzing-src
- Feature documentation: https://docs.gitlab.com/ee/user/application_security/api_fuzzing/
- Images: https://gitlab.com/security-products/api-fuzzing
